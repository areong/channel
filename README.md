# channel

C++ inter-thread communication channels, similar to the buffered channels in go.

Channels are implemented with condition variables.

## Example

First create a channel for integer messages:

```c++
channel::Channel<int> channel;
```

Let one thread send messages:

```c++
int message = 1
channel.send(message);
```

Then the other thread receives messages:

```c++
int message;
channel.receive(message);
```

After sending all messages, the channel can be closed at anytime:

```c++
channel.close();
```

The receiving thread can keep receiving messages, until the channel is closed:

```c++
while (channel.receive(message))
{
    // A message is received.
}
// The channel is empty and closed. No message is received.
```

Please see [example.cpp](/examples/example.cpp) for the complete code.

## Features

The channel buffers the messages, which means sending message is a non-blocking operation, while receiving messages blocks. Closing channel is also a non-blocking operation. One can still receive messages from a closed channel until the channel becomes empty.

Currently the channel is designed for one sender and one receiver.
