#ifndef CHANNEL_CHANNEL_H_
#define CHANNEL_CHANNEL_H_

#include <condition_variable>
#include <mutex>
#include <queue>

namespace channel
{

template <class T>
class Channel
{
public:
    Channel() : closed(false) {};
    Channel(const Channel&) = delete;
    Channel(Channel&&) = delete;
    Channel& operator=(const Channel&) = delete;
    Channel& operator=(Channel&&) = delete;
    /**
     * Close the channel when destructing the channel.
     * See close() for details.
     */
    ~Channel();
    /**
     * Send a copy of the message into the channel.
     * @return True if the message is sent.
     *  False if the channel is already closed and the message
     *  is not sent.
     */
    bool send(const T& message);
    /**
     * Send the message into the channel.
     * @return True if the message is sent.
     *  False if the channel is already closed and the message
     *  is not sent.
     */
    bool send(T&& message);
    /**
     * Receive a message from the channel.
     * If there is message in the channel,
     * this function receives the message immediately.
     * If there is no message in the channel,
     * this function blocks until a message is received,
     * or until the channel is closed.
     * @return True if a message is received successfully.
     *  False if the channel is empty and closed, and thus
     *  there is no message received.
     */
    bool receive(T& message);
    /**
     * Close the channel such that no message can be sent.
     * This function returns immediately even if there is
     * message in the channel not yet received.
     */
    void close();

private:
    std::mutex mutex;
    std::condition_variable condition_variable;

    std::queue<T> queue;
    bool closed;
};

template <class T>
Channel<T>::~Channel()
{
    close();
}

template <class T>
bool Channel<T>::send(const T& message)
{
    std::lock_guard<std::mutex> lock(mutex);

    // If the channel is closed, do not send the message.
    if (closed)
    {
        return false;
    }

    // Push the message into the queue.
    // If the queue was empty, the receiver might be waiting.
    // Notify it.
    bool queue_was_empty = queue.empty();
    queue.push(message);
    if (queue_was_empty)
    {
        condition_variable.notify_one();
    }
    return true;
}

template <class T>
bool Channel<T>::send(T&& message)
{
    std::lock_guard<std::mutex> lock(mutex);

    // If the channel is closed, do not send the message.
    if (closed)
    {
        return false;
    }

    // Push the message into the queue.
    // If the queue was empty, the receiver might be waiting.
    // Notify it.
    bool queue_was_empty = queue.empty();
    queue.push(message);
    if (queue_was_empty)
    {
        condition_variable.notify_one();
    }
    return true;
}

template <class T>
bool Channel<T>::receive(T& message)
{
    std::unique_lock<std::mutex> lock(mutex);

    // If the queue is empty, first check if the channel is closed.
    // If the channel is closed, there will be no more message
    // could be received. Terminate receiving.
    // Else, wait for the sender notifying about the new message,
    // or wait until the channel becomes closed.
    if (queue.empty())
    {
        if (closed)
        {
            return false;
        }

        condition_variable.wait(lock, [this]{return !this->queue.empty() || this->closed;});

        // If the receiver wakes up because the channel becomes closed,
        // termitate receiving.
        if (closed)
        {
            return false;
        }
    }

    // Pop out the message from the queue.
    message = queue.front();
    queue.pop();
    return true;
}

template <class T>
void Channel<T>::close()
{
    std::lock_guard<std::mutex> lock(mutex);

    // Do nothing if the channel is already closed.
    if (closed)
    {
        return;
    }

    closed = true;

    // If the queue is empty when the channel becomes closed,
    // the receiver might still be waiting. Notify it.
    if (queue.empty())
    {
        condition_variable.notify_one();
    }
}

}

#endif  // CHANNEL_CHANNEL_H_