#include <chrono>
#include <iostream>
#include <thread>
#include <vector>
#include <channel/Channel.h>

int main(int argc, char** argv)
{
    // Create a channel for two threads to communicate.
    channel::Channel<int> channel;

    std::thread sender([&channel]
    {
        for (auto&& i : std::vector<int>({0, 1, 2, 3, 4}))
        {
            // Send a message to the channel.
            channel.send(i);
            std::cout << "Send: " << i << "\n";

            std::this_thread::sleep_for(std::chrono::seconds(1));
        }

        // Close the channel after sending all messages.
        // The sent messages can still be received even the channel is closed.
        // (You can disable the thread sleeping to see such result.)
        // New messages cannot be sent to a closed channel.
        channel.close();
        std::cout << "Close the channel.\n";
    });

    std::thread receiver([&channel]
    {
        int message;

        // Receive a message.
        while (channel.receive(message))
        {
            // If receive() returns true, a message is received.
            std::cout << "Receive: " << message << "\n";
        }

        // If receive() returns false, the channel is empty and closed.
        // No message is received.
    });

    sender.join();
    receiver.join();

    return 0;
}